
angular.module('kpilance')
    .component('createTask', {
       templateUrl: 'app/components/createTask/createTask.html',
       controller: CreateTaskCtrl,
       controllerAs: 'ctrl'
    });

function CreateTaskCtrl($state, TaskService) {

    let ctrl = this;

    ctrl.create = () => {
        TaskService
            .createTask(ctrl.newTask)
            .then((task) => {
                $state.go('task', {id: task.id});
            }).catch((error) => {
                ctrl.message = error.message;
            });
    };

}
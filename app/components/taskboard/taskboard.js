'use strict';

angular.module('kpilance.taskboard', ['infinite-scroll'])
	.component('taskboard', {
		templateUrl: 'app/components/taskboard/taskboard.html',
		controller: boardCtrl,
		controllerAs: 'ctrl'
	});


function boardCtrl(TaskService) {
	let ctrl = this;

	ctrl.tasks = [];
	let loadCount = 8;
	ctrl.busy = false;
	ctrl.loadMore = () => {
		if(ctrl.busy){
			return;
		}
		ctrl.busy = true;
		let last = ctrl.tasks.length;
		TaskService
			.getTasks(last, loadCount, ctrl.subject, ctrl.orderBy)
				.then((tasks) => {
					ctrl.tasks = ctrl.tasks.concat(tasks);
					ctrl.busy = false;
				})
				.catch(() => {
					ctrl.allContent = true;
					ctrl.busy = false;
				});
	};

	ctrl.update = () => {
		ctrl.allContent = false;
		ctrl.tasks = [];
		ctrl.loadMore();		
	};

}
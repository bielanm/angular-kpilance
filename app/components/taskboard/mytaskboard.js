'use strict';

angular.module('kpilance.taskboard')
    .component('myTaskboard', {
        templateUrl: 'app/components/taskboard/taskboard.html',
        controller: MyBoardCtrl,
        controllerAs: 'ctrl'
    });


function MyBoardCtrl(MyTaskService) {
    let ctrl = this;

    ctrl.tasks = [];
    let loadCount = 8;
    ctrl.loadMore = () => {
        let last = ctrl.tasks.length;
        MyTaskService
            .getUserTasks(last, loadCount, ctrl.subject, ctrl.orderBy)
            .then((tasks) => {
                ctrl.tasks = ctrl.tasks.concat(tasks);
            })
            .catch(() => {
                ctrl.allContent = true;
            });
    };

    ctrl.update = () => {
        ctrl.allContent = false;
        ctrl.tasks = [];
        ctrl.loadMore();
    };

}
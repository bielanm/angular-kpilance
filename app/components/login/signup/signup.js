'use strict';

angular.module('kpilance')
	.component('signUp', {
		templateUrl: 'app/components/login/signup/signup.html',
		controller: signUpCtrl,
		controllerAs: 'ctrl'
	});

function signUpCtrl(UserService, $state) {
	let ctrl = this;

	ctrl.signup = function() {

		UserService.createUser(ctrl.user)
			.then(() => {
				$state.go('dashboard', {}, {reload: true});
			})
			.catch((response) => {
				ctrl.message = response.data.error;
			});
	}

}

'use strict';

angular.module('kpilance')
    .component('payment', {
        templateUrl: 'app/components/account/payment/payment.html',
        controller: paymentCtrl,
        controllerAs: 'ctrl'
    });

function paymentCtrl(PaymentAccountService) {
    let ctrl = this;

    ctrl.createPaymentAccount = () => {
        //TODO
    };

    ctrl.$onInit = () => {
        PaymentAccountService
            .getCurrentAccount()
            .then((response) => {
                ctrl.paymentAccount = response.paymentAccount;
            });
    };
}
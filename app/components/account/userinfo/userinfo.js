'use strict';

angular.module('kpilance')
	.component('userinfo', {
		templateUrl: 'app/components/account/userinfo/userinfo.html',
		controller: userinfoCtrl,
		controllerAs: 'ctrl',
		bindings: {
			user: '<'
		}
	});
	
function userinfoCtrl($state, UserService, AuthService) {
	let ctrl = this;

	ctrl.increaseRating = () => {
		UserService
			.increaseRating(ctrl.user.username)
			.then((rating) => {
				ctrl.user.rating = rating;
			})
			.catch((error) => {
				ctrl.message = error.message;
			});
	};
	ctrl.decreaseRating = () => {
		UserService
			.decreaseRating(ctrl.user.username)
			.then((rating) => {
				ctrl.user.rating = rating;
			})
			.catch((error) => {
				ctrl.message = error.message;
			});
	};

	ctrl.isMine = () => {
		return AuthService
					.isMine(ctrl.user.username);
	};

	ctrl.change = () => {
		ctrl.newUser = angular.copy(ctrl.user);
		ctrl.changeMode = true;
	};

	ctrl.deleteAccount = () => {
		UserService
			.deleteUser()
			.then(() => {
				AuthService.reset();
				$state.go('^', {}, {reload: true});
			})
			.catch((error) => {
				ctrl.message = error.message;
			});
	};

	ctrl.save = () => {
		UserService
			.changeUser(ctrl.newUser)
			.then((user) => {
				ctrl.user = user;
				ctrl.changeMode = false;
				ctrl.newUser = null;
				$state.reload();
			})
			.catch((error) => {
				ctrl.message = error.message;
			});
	};

}
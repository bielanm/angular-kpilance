
angular.module('kpilance.files', ['angularFileUpload'])
    .component('fileUp', {
        templateUrl: 'app/components/fileup/fileUp.html',
        controller: fileUpCtrl,
        controllerAs: 'ctrl',
        bindings: {
            url: '@',
            button: '@'
        }
    });

function fileUpCtrl($state, FileUploader, API) {
    let ctrl = this;

    ctrl.onError = (item, response, status, headers) => {
        ctrl.message = response.data.error;
    };

    ctrl.onSuccess = () => {
        $state.reload();
    };

    ctrl.$onInit = () => {
        ctrl.uploader = new FileUploader({
            url: API + ctrl.url,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            method: 'POST',
            withCredentials: true,
            removeAfterUpload: true,
            queueLimit: 1,
            onErrorItem: ctrl.onError,
            onCompleteAll: ctrl.onSuccess
        });
    }
}

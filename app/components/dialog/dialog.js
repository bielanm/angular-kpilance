'use strict';

angular.module('kpilance')
    .component('blockDialog', {
        templateUrl: 'app/components/dialog/dialog.html',
        transclude: true,
        controller: DialogCtrl,
        controllerAs: 'ctrl'
    });

function DialogCtrl($state) {
    let ctrl = this;

    ctrl.close = () => {
        $state.go('^');
    };
}
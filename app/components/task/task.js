
angular.module('kpilance')
    .component('task', {
        templateUrl: 'app/components/task/task.html',
        controller: TaskController,
        controllerAs: 'ctrl',
    });

function TaskController($state, $stateParams, TaskService, MyTaskService, AuthService, API) {

    let ctrl = this;
    ctrl.taskId = $stateParams.id;
    ctrl.API = API;

    ctrl.isMine = () => {
        if (ctrl.task && ctrl.task.author) {
            return AuthService.isMine(ctrl.task.author.username);
        } else {
            return false;
        }
    }

    ctrl.save = () => {
        TaskService
            .changeTask(ctrl.newTask)
            .then((task) => {
                ctrl.task = task;
                ctrl.changeMode = false;
                ctrl.newTask = null;
                $state.reload();
            })
            .catch((error) => {
                ctrl.message = error.message;
            });
    };

    ctrl.goToUser = (user) => {
        $state.go('user', {username: user.username});
    };

    ctrl.acceptUser = (user) => {
        MyTaskService
            .assignTask(ctrl.task.id, user.username)
            .then(() => {
               $state.reload();
            }).catch((error) => {
                 ctrl.message = error.message;
        });
    };

    ctrl.unassignUser = () => {
        MyTaskService
            .unassignTask(ctrl.task.id)
            .then(() => {
                $state.reload();
            }).catch((error) => {
            ctrl.message = error.message;
        });
    };

    ctrl.deleteDescription = () => {
        TaskService
            .deleteDescription(ctrl.task.id)
            .then(() => {
                $state.reload();
            }).catch((error) => {
                ctrl.message = error.message;
        });
    };


    ctrl.downloadDescription = () => {
        TaskService
            .downloadDescription(ctrl.task.id)
            .catch((error) => {
                ctrl.message = error.message;
            });
    };

    ctrl.downloadSolution = () => {
        TaskService
            .downloadSolution(ctrl.task.id)
            .catch((error) => {
                ctrl.message = error.message;
            });
    };

    ctrl.deleteSolution = () => {
        TaskService
            .deleteSolution(ctrl.task.id)
            .then(() => {
                $state.reload();
            }).catch((error) => {
            ctrl.message = error.message;
        });
    };

    ctrl.change = () => {
        ctrl.newTask = angular.copy(ctrl.task);
        ctrl.changeMode = true;
    };

    ctrl.delete = () => {
        TaskService
            .deleteTask(ctrl.task.id)
            .then(() => {
                $state.go("^");
            }).catch((error) => {
            ctrl.message = error.message;
        });
    };

    ctrl.endMyTask = () => {
        //TODO
    };

    ctrl.done = () => {
        //TODO
    };

    ctrl.assign = () => {
        TaskService
            .assignTask(ctrl.task.id)
            .then(() => {
                $state.reload();
            }).catch((error) => {
            ctrl.message = error.message;
        })
    };

    ctrl.unassign = () => {
        TaskService
            .unassignTask(ctrl.task.id)
            .then(() => {
                $state.reload();
            }).catch((error) => {
                ctrl.message = error.message;
        })
    };

    ctrl.$onInit = () => {
        TaskService
            .getTask(ctrl.taskId)
            .then((task) => {
                ctrl.task = task;
            }).catch((error) => {
                ctrl.message = error.message;
        })
    };

}
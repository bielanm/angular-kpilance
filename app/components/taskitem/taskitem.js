'use strict';

angular.module('kpilance.taskboard')
	.component('taskItem', {
		templateUrl: 'app/components/taskitem/taskitem.html',
		controller: ItemCtrl,
		controllerAs: 'ctrl',
		bindings: {
			details: '<'
		}
	});

function ItemCtrl($state) {
	let ctrl = this;

	ctrl.taskDialog = () => {
		$state.go('task', {id: ctrl.details.id});
	}
}


angular.module('kpilance')
    .component('user', {
        templateUrl: 'app/components/user/user.html',
        controller: UserCtrl,
        controllerAs: 'ctrl'
    });

function UserCtrl($stateParams, UserService) {
    let ctrl = this;

    ctrl.$onInit = () => {
        UserService
            .getUserDetails($stateParams.username)
            .then((user) => {
                ctrl.user = user;
            }).catch((error) => {
                ctrl.message = error.message;
        });
    }
}
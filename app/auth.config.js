'use strict';

angular.module('kpilance.auth', ['ui.router', 'ngStorage'])
	.factory('httpInterceptor', function ($state) {
		return {
			response: (response) => {
				return response;
			},
			responseError: (response) => {
				let error;
				if(response.status == 401) {
					$state.go('login');
				} else if(response.status == 403) {
					error = "You don't have access for this action!";
				}if(response.status == 400) {
					error = response.data.error
				} else if(response.status > 400) {
					error = "Some server side problems, try again please!";
				}

				if(response.status >= 400) throw new Error(error);

				return response;
			}
		};
	})
	.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

		let homeState = {
			name: 'dashboard',
			url: '/dashboard',
			component: 'dashboard',
			resolve: {
				currentUser: (AuthService) => {
					return AuthService.authUser();
				}
			}
		};

		let loginState = {
			parent: homeState,
			name: 'login',
			url: '/login',
			templateUrl: 'app/components/login/login.html'
		};

		let accountState = {
			parent: homeState,
			name: 'account',
			url: '/account',
			templateUrl: 'app/components/account/account.html',
			resolve: {
				currentUser: (AuthService) => {
					return AuthService.authUser();
				}
			}
		};
  
		let taskState = {
			parent: homeState,
			name: 'task',
			url: '/task/:id',
			component: 'task',
		};

		let userState = {
			parent: homeState,
			name: 'user',
			url: '/userinfo/:username',
			component: 'user',
			params: {
				username: ''
			}
		};

		let myTaskState = {
			parent: homeState,
			name: 'mytask',
			url: '/mytask',
			templateUrl: 'app/components/taskboard/mytask.html'
		};

		let createTaskState = {
			parent: homeState,
			name: 'createTask',
			url: '/new',
			component: 'createTask'
		};

		$stateProvider.state(loginState);
		$stateProvider.state(homeState);
		$stateProvider.state(userState);
		$stateProvider.state(accountState);
		$stateProvider.state(taskState);
		$stateProvider.state(myTaskState);
		$stateProvider.state(createTaskState);
		
		$urlRouterProvider.otherwise('/dashboard');

		$httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		$httpProvider.defaults.withCredentials = true;

		$httpProvider.interceptors.push('httpInterceptor');
	});

'use strict';

angular.module('kpilance.service')
    .service('MyTaskService', function($http, API) {

        this.getUserTasks = (from, quantity, subject, orderBy) => {
            return $http({
                url: API + '/usertask',
                method: 'GET',
                params: {
                    from: from,
                    quantity: quantity,
                    subject: subject,
                    order_by: orderBy
                }
            }).then((response) => {
                return response.data.tasks;
            });
        };

        this.getAssignTasks = (from, quantity, subject, orderBy) => {
            return $http({
                url: API + '/usertask/assign',
                method: 'GET',
                params: {
                    from: from,
                    quantity: quantity,
                    subject: subject,
                    order_by: orderBy
                }
            }).then((response) => {
                return response.data.tasks;
            });
        };

        this.assignTask = (id, username) => {
            return $http({
                url: API + '/user/task/' + id + '/assign',
                method: 'PUT',
                data: {
                    username: username
                }
            });
        };

        this.unassignTask = (id) => {
            return $http({
                url: API + '/user/task/' + id + '/unassign',
                method: 'PUT'
            });
        };

        this.endTask = (id) => {
            return $http({
                url: API + '/user/task/' + id + '/done',
                method: 'PUT'
            });
        };

        // this.uploadDescriptionFile = (id) => {
        //     return $hhtp({
        //         url: API + '/task/' + task.id + '/file',						//TODO
        //         method: 'POST'
        //     });
        // };
        //
        // this.downloadDescriptionFile = (id) => {
        //     return $hhtp({
        //         url: API + '/task/' + task.id + '/file',						//TODO
        //         method: 'POST'
        //     });
        // };

    });
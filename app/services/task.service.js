'use strict';

angular.module('kpilance.service')
	.service('TaskService', function($http, API) {

		this.createTask = (task) => {
			return $http({
				url: API + '/task',
				method: 'POST',
				data: task
			}).then((response) => {
				return response.data;
			});
		};

		this.getTask = (id) => {
			return $http({
				url: API + '/task/' + id,
				method: 'GET'
			}).then((response) => {
				return response.data;
			});
		};

		this.changeTask = (task) => {
			return $http({
				url: API + '/task/' + task.id,
				method: 'PUT',
				data: task
			}).then((response) => {
				return response.data;
			});
		};

		this.deleteTask = (id) => {
			return $http({
				url: API + '/task/' + id,
				method: 'DELETE'
			});
		};

		this.getTasks = (from, quantity, subject, orderBy) => {
			return $http({
				url: API + '/task',
				method: 'GET',
				params: {
					from: from,
					quantity: quantity,
					subject: subject,
					order_by: orderBy
				}
			}).then((response) => {
				return response.data.tasks;
			});
		};

		this.assignTask = (id) => {
			return $http({
				url: API + '/task/' + id + '/assign',
				method: 'PUT'
			});
		};

		this.unassignTask = (id) => {
			return $http({
				url: API + '/task/' + id + '/unassign',
				method: 'PUT'
			});
		};

		this.deleteDescription = (id) => {
        	return $http({
				url: API + '/task/' + id + '/description',
				method: 'DELETE'
			});
        };

	    this.downloadDescription = (id) => {
	    	return $http({
				url: API + '/task/' + id + '/description',
				method: 'GET'
			});  
	    };

	    this.downloadSolution = (id) => {
	    	return $http({
				url: API + '/task/' + id + '/solution',
				method: 'GET'
			}); 
	    };

	    this.deleteSolution = (id) => {
	        return $http({
				url: API + '/task/' + id + '/solution',
				method: 'DELETE'
			}); 
	    };

	});
'use strict';

angular.module('kpilance.service', [])
	.service('AuthService', function($http, $localStorage, API) {

		this.login = (user) => {
			return $http({
				url: API + '/login',
				method: 'POST',
				data: user
			}).then((response) => {
				$localStorage.currentUser = response.data;
				return response.data;
			});
		};

		this.logout = () => {
			return $http({
				url: API + '/logout',
				method: 'DELETE'
			}).then(() => {
				$localStorage.currentUser = null;
			}).catch(() => {
				$localStorage.currentUser = null;
			});

		};

		this.authUser = () => {
			return $localStorage.currentUser;
		};

		this.reset = () => {
			$localStorage.currentUser = null;
		};

		this.isMine = (name) => {
			return !!($localStorage.currentUser && $localStorage.currentUser.username == name);
		}

	});
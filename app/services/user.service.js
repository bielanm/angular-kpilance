'use strict'

angular.module('kpilance.service')
	.service('UserService', function($http, $localStorage, API) {

		this.createUser = (user) => {
			return $http({
				url: API + '/user',
				method: 'POST',
				data: user
			}).then((response) => {
				$localStorage.currentUser = response.data;
				return response.data;
			});
		};

		this.deleteUser = () => {
			return $http({
				url: API + '/user',
				method: 'DELETE',
			});
		};

		this.changeUser = (user) => {
			return $http({
				url: API + '/user',
				method: 'PUT',
				data: user
			}).then((response) => {
				$localStorage.currentUser = response.data;
				return response.data;
			});
		};

		this.getUserDetails = (username) => {
			return $http({
				url: API + '/user/' + username,
				method: 'GET'
			}).then((response) => {
				return response.data;
			});
		};

		this.increaseRating = (username) => {
			return $http({
				url: API + '/' + username + '/increase',
				method: 'POST'
			}).then((response) => {
				return response.data.rating;
			});
		};

		this.decreaseRating = (username) => {
			return $http({
				url: API + '/' + username + '/decrease',
				method: 'POST'
			}).then((response) => {
				return response.data.rating;
			});
		};

	});
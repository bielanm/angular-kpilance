'use strict';

angular.module('kpilance.backend', ['ngMockE2E'])
    .run(function($httpBackend) {

        let error = {
            error: "Error message from serverMock!"
        };

        //Auth API

        let qwertyCredentials = {
            username: 'qwerty',
            password: 'qwerty'
        };

        let qwerty = {
            username: 'qwerty',
            email: 'qwerty@gmail.com',
            photo_url: 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiyipv-mvzQAhWGFSwKHQzfCFoQjRwIBw&url=http%3A%2F%2Fpumainthailand.com%2Fpage%2F12%2F&bvm=bv.142059868,d.bGg&psig=AFQjCNH2Iea1KNO_Ihq_yN9Ro6wzGavoAg&ust=1482097874803925',
            rating: 0
        };

        let mishaCredentials = {
            username: 'misha',
            password: 'qwerty'
        };

        let misha = {
            username: 'misha',
            email: 'bielanm.box@gmail.com',
            photo_url: 'https://pp.vk.me/c627321/v627321190/1f046/Yzd4lK_Alfk.jpg',
            rating: 113
        };

        let lyudaCredentials = {
            username: 'lyuda',
            password: 'qwerty'
        };

        let lyuda = {
            username: 'lyuda',
            email: 'lyudasmithk@gmail.com',
            photo_url: 'https://pp.vk.me/c636824/v636824485/b95b/J6PQ1e9BCTw.jpg',
            rating: 113
        };

        $httpBackend.whenPOST('/login', qwertyCredentials).respond(200, qwerty);
        $httpBackend.whenPOST('/login', mishaCredentials).respond(200, misha);
        $httpBackend.whenPOST('/login', lyudaCredentials).respond(200, lyuda);

        $httpBackend.whenDELETE('/logout').respond(200);

        //User API

        let qwertyCreate = {
            username: 'qwerty',
            email: 'qwerty@gmail.com',
            password: 'qwerty',
            posswordConfirm: 'qwerty'
        };

        let qwertyPUT = {
            username: 'misha',
            email: 'misha@gmail.com',
            photo_url: 'misha'
        };

        let qwertyAfterPUT = {
            username: 'lololo',
            email: 'qwerty@gmail.com',
            photo_url: 'https://pp.vk.me/c625422/v625422190/41753/f_f85eAQ0g4.jpg',
            rating: 0
        };

        $httpBackend.whenGET('/user/' + misha.username).respond(200, misha);
        $httpBackend.whenGET('/user/' + qwerty.username).respond(200, qwerty);
        $httpBackend.whenPOST('/user', qwertyCreate).respond(201, qwerty);
        $httpBackend.whenPUT('/user', qwertyPUT).respond(200, qwertyAfterPUT);
        $httpBackend.whenDELETE('/user').respond(200);


        //Task API

        let taskPOST = {
            title: 'qwerty',
            description: 'qwerty',
            subject: 'math',
            price: '99',
            estimate: '99'
        };

        let taskPOSTWith401 = {
            title: 'failed401',
            description: 'failed401',
            subject: 'math',
            price: '99',
            estimate: '99'
        };

        let taskPOSTWith400 = {
            title: 'failed400',
            description: 'failed400',
            subject: 'math',
            price: '99',
            estimate: '99'
        };

        let taskPUT = {
            id: 1,
            title: 'qwerty',
            description: 'qwerty',
            subject: 'math',
            price: '99',
            estimate: '99'
        };

        $httpBackend.whenPOST('/task', taskPOST).respond(201, new MyTaskDetails());
        $httpBackend.whenPOST('/task', taskPOSTWith401).respond(401, error);
        $httpBackend.whenPOST('/task', taskPOSTWith400).respond(400, error);
        $httpBackend.whenPUT('/task/1', taskPUT).respond(200, new TaskDetails());

        var count = 0;
        function TaskItem() {

            let titles = ['Web site on angular', 'Math homeworks', 'Html layout from PSD'];
            let subjects = ['math', 'physics', 'languages', 'programming'];

            return {
                id: count++,
                title: titles[Math.floor(Math.random() * 3)],
                subject: subjects[Math.floor(Math.random() * 4)],
                price: Math.floor(Math.random() * 100),
                estimate: Math.floor(Math.random() * 100),
                created_at: '14 december',
                solution: false
            };
        }

        function MyOfferedTaskItem() {
            let task = new TaskItem();
            task.offered = true;
            task.users = [{
                username: 'misha',
                email: 'bielanm.box@gmail.com'
            }, {
                username: 'lyuda',
                email: 'bielanm.box@gmail.com'
            }];
            return task;
        }
        
        function MyTaskItemWithEmployee() {
            let task = new MyOfferedTaskItem();
            task.employee = {
                username: 'misha',
                email: 'bielanm.box@gmail.com'
            };
            return task;
        }

        function MyTaskItemWithSolution() {
            let task = new MyTaskItemWithEmployee();
            task.solution = true;
            return task;
        }

        function MyTaskItemWithSolution() {
            let task = new MyTaskItemWithEmployee();
            task.solution = true;
            return task;
        }

        function TaskDetails() {

            let task = new TaskItem();
            let descriptions = ['Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels chronicle the life of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, all of whom are students at Hogwarts School of Witchcraft and Wizardry.',
                                'Гол Гонсало Игуаина в первой половине встречи стал единственным в матче и принес Старой синьоре минимальную победу (1:0).',
                                'JavaScript — прототипно-ориентированный сценарный язык программирования. Является реализацией языка ECMAScript (стандарт ECMA-262[6])'];

            task.description = descriptions[Math.floor(Math.random() * 3)];
            task.author = {
                username: 'misha',
                email: 'bielanm.box@gmail.com'
            };
            task.attached = true;
            return task;
        }

        function MyTaskDetails() {

            let task = new TaskDetails();
            task.offered = false;
            task.users = [];
            task.solution = false;

            return task;
        }

        function MyOfferedTaskDetails() {

            let task = new MyTaskDetails();
            task.offered = true;
            task.users = [{
                username: 'misha',
                email: 'bielanm.box@gmail.com'
            }, {
                username: 'qwerty',
                email: 'qwerty@gmail.com'
            }];

            return task;
        }

        function MyInProgressTaskDetails() {

            let task = new MyOfferedTaskDetails();
            task.employee = {
                username: 'qwerty',
                email: 'qwerty@gmail.com'
            };

            return task;
        }


        //userinfo/task
        let myTasks = [];
        for(let i = 0; i < 6; i++){
            let task = null;
            let taskDetails = null;
            if(i == 2 || i == 5) {
                task = new MyOfferedTaskItem();
                taskDetails = new MyOfferedTaskDetails();
            } else if(i == 4){
                task = new MyTaskItemWithEmployee();
                taskDetails = new MyInProgressTaskDetails();
            } else if(i == 1){
                task = new MyTaskItemWithSolution();
                taskDetails = new MyInProgressTaskDetails();
            } else {
                task = new TaskItem();
                taskDetails = new TaskDetails();
            }
            task.id = i;
            taskDetails.id = i;
            myTasks.push(task);
            $httpBackend.whenGET('/task/' + i).respond(200, taskDetails);
        }
        $httpBackend.whenGET('/usertask?from=0&quantity=8&subject=all&order_by=price').respond(200, {
            tasks: myTasks
        });

        //task
        let tasks = [];
        for(let i = 0; i < 8; i++){
            let task = new TaskItem();
            let taskDetails = new TaskDetails();
            task.id = i;
            taskDetails.id = i;
            tasks.push(task);
            $httpBackend.whenGET('/task/' + i).respond(200, taskDetails);
        }
        $httpBackend.whenGET('/task?from=0&quantity=8&subject=all&order_by=price').respond(200, {
            tasks: tasks
        });
        $httpBackend.whenGET('/task?from=8&quantity=8&subject=all&order_by=price').respond(200, {
            tasks: tasks
        });
        $httpBackend.whenGET('/task?from=16&quantity=8&subject=all&order_by=price').respond(200, {
            tasks: tasks
        });
        $httpBackend.whenGET('/task?from=0&quantity=8&subject=math&order_by=price').respond(200, {
            tasks: tasks
        });

        //
        $httpBackend.whenGET(/.html/).passThrough();
    });